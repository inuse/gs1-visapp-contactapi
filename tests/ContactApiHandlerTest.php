<?php 

/**
* Unit tests for the contact API handler class.
*/
namespace Gs1visapp\ContactAPI;

require __DIR__ . '/../vendor/autoload.php';

use Gs1visapp\ContactAPI\ContactAPIHandler;

define('DEBUG', true);

// API Key (added to to URL as api_key GET parameter when the API call is made)
define('CONTACT_API_KEY', 'h6tjyyk5um4plkj45hg12cx3qsdf');

// API Key limit for number of returned countries - probably won't need to be modified (?)
define('CONTACT_API_LIMIT_PARAM', 200);

// API URL:
define('CONTACT_API_URL', 'http://gs1online.unidev.eu/staffdir_api/countries.php');

// Cache time for cache file with contact data, in seconds:
define('CONTACT_API_CACHE_TIME', 10); // 10 seconds

// Cache file path for the on-disk JSON file with the cached data:
define('CONTACT_API_CACHE_FILE_PATH', __DIR__.'/_contact_data_cache.json');

define('ORIGINAL_DATA', '[{"country_name":"Albania","iso":"AL","id":"6","name":"GS1 Albania","address_line_1":"Rruga e \"Barrikadave\"","zipcode":"-","city":"Tirana","mo_phone":"+ 355 4 232073","fax":"+ 355 4 232073","website":"http:\/\/www.gs1al.org","mo_email":"info@gs1al.org"}]');

class ContactApiHandlerTest extends \PHPUnit_Framework_TestCase
{
    public function restoreCacheFile() {
        file_put_contents(CONTACT_API_CACHE_FILE_PATH, ORIGINAL_DATA);
    }

    public function emptyCacheFile() {
        file_put_contents(CONTACT_API_CACHE_FILE_PATH, '');
    }

    public function touchCacheFile($time) {
        touch(CONTACT_API_CACHE_FILE_PATH, $time);
        clearstatcache();
    }

    public function deleteCacheFile() {
        unlink(CONTACT_API_CACHE_FILE_PATH);
    }
    public function testConstructor() {

        $c = new ContactAPIHandler(
            'http://example.com',
            'abc',
            100,
            3,
            CONTACT_API_CACHE_FILE_PATH
            );

        $this->assertInstanceOf('Gs1visapp\ContactAPI\ContactAPIHandler', $c);
        $this->assertEquals('http://example.com?api_key=abc&limit=100', $c->API_URL);
        $this->assertEquals(3, $c->cacheDelta);
        $this->assertEquals(CONTACT_API_CACHE_FILE_PATH, $c->cacheFilePath);
    }

    public function testFileExists() {

        $this->restoreCacheFile();

        $c = new ContactAPIHandler(
            CONTACT_API_URL,
            CONTACT_API_KEY,
            CONTACT_API_LIMIT_PARAM,
            CONTACT_API_CACHE_TIME,
            CONTACT_API_CACHE_FILE_PATH
            );
        $this->assertTrue($c->fileExists());
        $this->emptyCacheFile();
        $this->assertFalse($c->fileExists());
        $this->restoreCacheFile();
        $this->assertTrue($c->fileExists());
        $this->deleteCacheFile();
        $this->assertFalse($c->fileExists());
        $time = time();
        $this->touchCacheFile($time);
    }
    public function testFileIsFresh() {
        // This creates a fake cache file:
        $this->restoreCacheFile();

        // Set up the cache file modification dates:
        $time = time() - 60; // One minute since last modification.
        $this->touchCacheFile($time); // ...is set here.

        // Create the API instance.
        $c = new ContactAPIHandler(
            CONTACT_API_URL,
            CONTACT_API_KEY,
            CONTACT_API_LIMIT_PARAM,
            30, // This param sets cache stale limit as max 30 seconds old.
            CONTACT_API_CACHE_FILE_PATH
            );

        // By now, the cache file is 1 minute old as set above.
        $this->assertFalse($c->fileIsFresh());

        // Touching the cache file with current time should make it fresh. 
        $this->touchCacheFile(time());
        $this->assertTrue($c->fileIsFresh());
    }
    public function testTouchFile() {
        // Create the API instance.
        $this->deleteCacheFile();
        $this->restoreCacheFile();
        $c = new ContactAPIHandler(
            CONTACT_API_URL,
            CONTACT_API_KEY,
            CONTACT_API_LIMIT_PARAM,
            30, // This param sets cache stale limit as max 30 seconds old.
            CONTACT_API_CACHE_FILE_PATH
            );
        // At this point, file is not fresh.
        $this->touchCacheFile(time() - 1000);
        $this->assertFalse($c->fileIsFresh());
        $c->touchFile();
        $this->assertTrue($c->fileIsFresh());
    }

    public function testGetDataFromApi() {
        $constructor_args = array(
                'http://example.com',
                'foo',
                CONTACT_API_LIMIT_PARAM,
                30,
                CONTACT_API_CACHE_FILE_PATH
            );
        $c = $this->getMockBuilder('Gs1visapp\ContactAPI\ContactAPIHandler')
                  ->setConstructorArgs($constructor_args)
                  ->setMethods(array('httpRequest'))
                  ->getMock();

        $c->method('httpRequest')
          ->will($this->onConsecutiveCalls(
                '',
                '[]',
                '[{"foo": "bar"}]',
                '[{"foo": "bar"}, {baz: "fizz"}]')); // last one is bad json, no quotes on last key.
        // First call should return null, since the data is empty.
        $this->assertEquals($c->getDataFromAPI(), null);

        // Second call should also return null, since the data is too short.
        $this->assertEquals($c->getDataFromAPI(), null);

        // Third call should return array:
        $this->assertEquals($c->getDataFromAPI(), array(0=>array('foo'=>'bar')));

        // Third call should return null, since the JSON is invalid:
        $this->assertEquals($c->getDataFromAPI(), null);
    }

    public function testGetDataFromFile() {
        $this->restoreCacheFile();

        $c = new ContactAPIHandler(
            'http://example.com',
            'abc',
            100,
            3,
            CONTACT_API_CACHE_FILE_PATH
            );
        $result = $c->getDataFromFile();
        $this->assertEquals($result, json_decode(ORIGINAL_DATA, true));
        $this->deleteCacheFile();
        $this->touchCacheFile(time());
        $result = $c->getDataFromFile();
        $this->assertEquals($result, array());
        $this->restoreCacheFile();
        file_put_contents(CONTACT_API_CACHE_FILE_PATH, '[{foo: "bar}]'); // Bad JSON
        $result = $c->getDataFromFile();
        $this->assertEquals($result, array());
    }

    public function testGetData() {
        // No cache file to begin with:
        $this->deleteCacheFile();

        // --- Create mocked API instance:
        $constructor_args = array(
                'http://example.com',
                'foo',
                CONTACT_API_LIMIT_PARAM,
                30, // 30 second cache limit.
                CONTACT_API_CACHE_FILE_PATH
            );
        $c = $this->getMockBuilder('Gs1visapp\ContactAPI\ContactApiHandler')
                  ->setConstructorArgs($constructor_args)
                  ->setMethods(array('httpRequest'))
                  ->getMock();

        $c->method('httpRequest')
          ->will($this->onConsecutiveCalls(
                '',
                '[]',
                '[{"foo":"bar"}]',
                '[{"foo":"bar"}, {baz:"fizz"}]',
                '[{"foo":"bar"}]')); 

        // -- end mock creation.
        $c->debug_log('TestGetData-----');
        // First time, we get empty response, and consequently null, 
        // which turns into empty array in getData
        $this->assertEquals($c->getData(), array());

        $c->debug_log('2nd-----');

        // Second time, we get too short response, and consequently null, 
        // which turns into empty array in getData
        $this->assertEquals($c->getData(), array());
        
        $c->debug_log('3rd-----');
        // Third time, we get actual data
        $this->assertEquals($c->getData(), array(0=>array('foo'=>'bar')));

        // At this point, the valid data should also be written to file:
        $this->assertEquals(file_get_contents(CONTACT_API_CACHE_FILE_PATH), '[{"foo":"bar"}]');
        
        $c->debug_log('4th-----');

        $this->touchCacheFile(time() - 600);
        // The next request returns bad Ajax, and should not overwrite the correct
        // data in the cache file:
        $result = $c->getData();
        $c->debug_log(print_r($result, true));
        $this->assertEquals(file_get_contents(CONTACT_API_CACHE_FILE_PATH), '[{"foo":"bar"}]');
        
        $c->debug_log('5th-----');
        // The next call should return the last good data, so we fake that the file
        // modification date is older:
        $this->touchCacheFile(time() - 600);
        // Cache file is now stale:
        $this->assertFalse($c->fileIsFresh());
        // Now the API call should compare data, find that they are the same,
        // and then update the file mdate.
        $c->getData();
        $this->assertEquals(file_get_contents(CONTACT_API_CACHE_FILE_PATH), '[{"foo":"bar"}]');
        $this->assertTrue($c->fileIsFresh());

        // This one shouldn't touch the HTTP thing, since file is fresh.
        $c->getData();
        $this->assertEquals(file_get_contents(CONTACT_API_CACHE_FILE_PATH), '[{"foo":"bar"}]');
    }
}


?>
