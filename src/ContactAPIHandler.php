<?php 

namespace Gs1visapp\ContactAPI;

/**
* Small class to handle a cached API proxy, basically to get JSON data from
* the contact details API of GS1 only if the data on file doesn't 
* exist or is getting too old.
*/
class ContactAPIHandler
{

    public $API_URL;
    public $APIKey;
    public $cacheDelta;
    public $cacheFilePath;
    public $dataOnFile;
    private $debug;

    public function __construct($API_URL, $APIkey, $APILimit, $cacheDelta, $cacheFilePath, $debug=false)
    {
        $this->API_URL = $API_URL . '?api_key=' . $APIkey . '&limit=' . $APILimit;
        $this->cacheDelta = $cacheDelta;
        $this->cacheFilePath = $cacheFilePath;
        $this->dataOnFile = false;
        $this->debug = $debug;
    }
    /**
    * Debug logger
    */
    public function debug_log($msg) {
        if ($this->debug || defined('DEBUG') && DEBUG)
        {
            error_log($msg);
        }
    }

    /**
     * Method to check wether the cache file exists or not.
     * @return Boolean
     */
    public function fileExists()
    {
        $path = $this->cacheFilePath;
        // Bit of a magic number in here, but we don't want an empty file to
        // give a false positive that the file exists as cached,
        // so we check if the file has contents that give it a length over 2
        // characters (i.e. something more than at least "[]")
        // Clear the internal cache in case we need to check filesize again later.
        clearstatcache();

        return file_exists($path) && is_readable($path) && filesize($path) > 2;
    }

    /**
    * Method to check if file is newer than the cacheTime cutoff.
    * @return Boolean
    */
    public function fileIsFresh()
    {
        $dateNow = time();
        $fmodified = filemtime($this->cacheFilePath);
        return $dateNow - $fmodified < $this->cacheDelta;
    }

    /**
    * Method to update the "modified date" on the cache file, if it's unchanged
    * but still fresh.
    * @return Int
    */
    public function touchFile()
    {
        $mod = touch($this->cacheFilePath);
        clearstatcache();
        if ($mod) {
            $this->debug_log('[GS1 Visapp]: Cache file updated!');
        }
        return $mod;
    }

    /**
     * Method that wraps the HTTP-handling bits. Delegates to either cURL or
     * file_get_contents depending on what's available on the current system.
     * @return String or False
     */
    public function httpRequest($url)
    {

        $result = false;
        // Delegate fetching to curl if it is installed
        // (since it's supposedly faster:)
        if (function_exists('curl_exec') 
                && function_exists('curl_setopt') && function_exists('curl_init')) {
            $curl_handler = curl_init();
            
            // Set curl to not output response headers
            curl_setopt($curl_handler, CURLOPT_HEADER, 0);

            //Set curl to return the data instead of printing it to the browser.
            curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, 1); 

            // 
            curl_setopt($curl_handler, CURLOPT_URL, $url);
        
            $result = curl_exec($curl_handler);

            if (!$result) {
                $this->debug_log('[GS1 Visapp]: No results from HTTP call! cURL error: '. curl_error($curl_handler));
            }
            
            curl_close($curl_handler);
        }
        else if (function_exists('file_get_contents')) {

            // custom error handler to suppress warnings:
            function silentWarning($errno, $errstr) {
                error_log('[GS1 Visapp]: Error in get_file_contents - ' . $errstr);
            }
            // set silentWarning to handle warnings:
            set_error_handler('silentWarning', E_WARNING);
            $result = file_get_contents($url);

            // Back to the default error handler.
            restore_error_handler();
        }
        return $result;
    }

    /**
     * Method for calling the API, returning the response converted
     * to a PHP array.
     * @return Array or Bool
     */
    function getDataFromAPI()
    {
        $this->debug_log('[GS1 Visapp]: Getting data from API');
        $this->debug_log('[GS1 Visapp]: API URL: ' . $this->API_URL);

        $url = $this->API_URL;

        // Set this to null to be consistent with bad data etc.
        $data = null;

        $result = $this->httpRequest($url);

        if (!$result) {
            $data = null;
        } else {
            $this->debug_log('[GS1 Visapp]: Found result: ' . print_r($result, true));
            if (trim($result) !== '[]') {
                $this->debug_log('[GS1 Visapp]: has data!');
                $data = $result;
                $this->debug_log('[GS1 Visapp]: '.$data);
            } else {
                $this->debug_log('[GS1 Visapp]: Data is empty...');
                $data = null;
            }
        }
        // Return json_decode of data: should return null if data is an invalid 
        // string or any other value that cannot be decoded.
        // Attempts to return directly if data is still null.
        return ($data == null)? $data: json_decode($data, true);
    }

    /**
     * Method for getting contact data from file on disk.
     * @return Array
     */
    public function getDataFromFile()
    {
        $this->debug_log('[GS1 Visapp]: Getting data from file]');
        // Initialize result to null (same as result of failed json_decode)
        $result = null;

        $contents = file_get_contents($this->cacheFilePath);

        // Store contents of JSON file as associative array.
        // (Second parameter to json_decode sets wether it should 
        // be associative or not.)
        $result = json_decode($contents, true);
        
        // json_encode will have either converted to an array or returned
        // null as the result. Either return $result if truthy, but otherwise
        // we'd want to return an empty array for consistency's sake.
        if ($result) {
            return $result;
        } else {
            return array();
        }
    }

    /**
     * Writes PHP array to disk as JSON string.
     * @return Boolean
     */
    private function writeToFile($data)
    {
        $this->debug_log('[GS1 Visapp]: writing data to file');
        $done = file_put_contents($this->cacheFilePath, json_encode($data));

        return (boolean) $done;
    }

    /**
    * Facade that delegates fetching of contact data to either
    * API or file, depending on wether the file data is fresh enough.
    * @return Array
    */
    public function getData()
    {
        // check if the file with contact information exists
        if ($this->fileExists()) {
            // Return early with data on file if it is fresh.
            if ($this->fileIsFresh()) {
                $this->debug_log('[GS1 Visapp]: File is fresh. Return getDataFromFile.');
                return $this->getDataFromFile();
            }
            $this->debug_log('[GS1 Visapp]: File exists, but is not fresh.');
            // If not fresh, read the data from file and store it as a variable.
            $this->dataOnFile = $this->getDataFromFile();
        }

        $newData = $this->getDataFromAPI();

        $this->debug_log('[GS1 Visapp]: '.print_r($newData, true));
        // At this point, we should have either JSON data or null. 
        // If falsy, fail with either old data or empty array.
        if (!$newData) {
            $this->debug_log('[GS1 Visapp]: No new data received');
            if ($this->dataOnFile) {
                $this->debug_log('[GS1 Visapp]: still have data on file.');
                return $this->dataOnFile;
            } else {
                $this->debug_log('[GS1 Visapp]: no data on file. Returning empty.');
                return array();
            }
        }

        // Check if the new data is not the same as the one on disk.
        if ($newData != $this->dataOnFile) {
            $this->debug_log('[GS1 Visapp]: new data!]');
            $this->writeToFile($newData);
        } else {
            $this->debug_log('[GS1 Visapp]: same data!]');
            $this->touchFile();
        }

        return $newData;
    }
}
